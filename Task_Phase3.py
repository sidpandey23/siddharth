import parsers as parser
import pandas as pd
import scipy
import operator
from scipy.spatial import distance

def main():
	imageDictionary = parser.parseImageFile()
	print("Constructed image dictionary...")
	rows, itemIndexMap = parser.processMapToDataFrame(imageDictionary,0)
	print("Started constructing the dataframe...")
	df = pd.DataFrame(rows)
	df['image_id'] = itemIndexMap
	df.set_index('image_id',inplace=True)
	df.fillna(0,inplace=True)
#	print(len(rows))
#	print(len(itemIndexMap))
#	print(df.loc['9067739127']['acropoli'])

	print(df.head())
	first_df = df
	second_df = df
	image_ids = first_df.index.tolist()
	cols = second_df.columns.tolist()
	
	print("\nImage IDs extracted : \n " + str(image_ids))
	print("\n\nColumns extracted : \n " + str(cols))
	distances = scipy.spatial.distance.cdist(first_df, second_df, metric='euclidean')
	print("\n\n")
	
	distances_list = []
	all_distances_list = []
	
	k = 11											##suppose we want to get 5 closest images
	distances_df = pd.DataFrame(distances)
	distances_df['image_id'] = image_ids
	distances_df.set_index('image_id',inplace=True)
	
	count = 0
	for index,row in distances_df.iterrows():		##go through each image and its row vector.
		distances = row.values
		print("\n\nWorking for image number " + str(count) + " image id " + str(index))         ##should give the image id as we have set that as an index.
		count += 1
		for i in range(0,len(distances)):			##get all the distance values.
			distances_list.append((image_ids[i],distances[i]))  ##(image_id,distance) map.
		distances_list = sorted(distances_list,key=operator.itemgetter(1)) ##sort in increasing order of Euclidean distances
		distances_list = distances_list[:k+1]  ##can set the slice to [:k+1] for debugging purposes.
		print("\nDistances for image with the image id " , str(index) , " are : " , str(distances_list))
		print("\n " , str(len(distances_list)))
		all_distances_list.append(distances_list[:k+1])
		print(all_distances_list[:2])
		distances_list.clear()
	
	print("\n\n")
	print(len(all_distances_list[0]))
	distances_df = pd.DataFrame(all_distances_list)
	distances_df['image_id'] = image_ids
	distances_df.set_index('image_id',inplace=True)
	print(distances_df.head())
	
	for index,row in distances_df.iterrows():
		print("Index : " + str(index))
		print("Rows : " + str(row))
'''
Verifying distances are correctly computed
	dist = set()
	for i in range(0,len(image_ids)):
		if(distances[i][i] > 0.0):
			print(distances[i][:])
			print()
			print(str(i) + " distance = " + str(distances[i][i])) 
		dist.add(distances[i][i])
	print(dist)
'''
	
if __name__ == '__main__':
	main()